USE [SuperHeroDB]
GO

INSERT INTO [dbo].[Assistant]
           ([Name]
           ,[Description]
           ,[SuperHeroID])
     VALUES
           ('Betty Brant',
           'Love interest',
           1),

		   ('J.A.R.V.I.S',
           'Computer',
           2),

		   ('Virginia "Pepper" Pots',
           'Assistant',
           2)
GO


