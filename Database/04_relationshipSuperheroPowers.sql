USE [SuperHeroDB]
GO

/****** Object:  Table [dbo].[SuperHeroPower]    Script Date: 24/08/2021 16:20:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[SuperHeroPower](
	[SuperHeroID] [int] NOT NULL,
	[PowerID] [int] NOT NULL,
 CONSTRAINT [PK_SuperHeroPower_1] PRIMARY KEY CLUSTERED 
(
	[SuperHeroID] ASC,
	[PowerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO



ALTER TABLE [dbo].[SuperHeroPower]  WITH CHECK ADD  CONSTRAINT [FK_SuperHeroPower_Power] FOREIGN KEY([PowerID])
REFERENCES [dbo].[Power] ([ID])
GO

ALTER TABLE [dbo].[SuperHeroPower] CHECK CONSTRAINT [FK_SuperHeroPower_Power]
GO

ALTER TABLE [dbo].[SuperHeroPower]  WITH CHECK ADD  CONSTRAINT [FK_SuperHeroPower_Superhero] FOREIGN KEY([SuperHeroID])
REFERENCES [dbo].[Superhero] ([ID])
GO

ALTER TABLE [dbo].[SuperHeroPower] CHECK CONSTRAINT [FK_SuperHeroPower_Superhero]
GO


