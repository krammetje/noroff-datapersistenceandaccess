USE [SuperHeroDB]
GO

INSERT INTO [dbo].[Power]
           ([Name]
           ,[Description])
     VALUES
           ('Webshooter',
		   'Shoots a web'),

		   ('Flying',
		   'Whoosh'),

		   ('Laser',
		   'Light amplification by stimulated emission of radiation'),

		   ('Super smash',
		   'Smashing')

INSERT INTO [dbo].[SuperHeroPower]
			([SuperHeroID],
			[PowerID])(
			SELECT [Superhero].[ID], [Power].[ID]
			FROM [dbo].[Superhero], [dbo].[Power] 
			WHERE Superhero.Alias = 'Spiderman' AND [Power].Name = 'Webshooter'
			);

INSERT INTO [dbo].[SuperHeroPower]
			([SuperHeroID],
			[PowerID])(
			SELECT [Superhero].[ID], [Power].[ID]
			FROM [dbo].[Superhero], [dbo].[Power] 
			WHERE (Superhero.Alias = 'Thor' OR Superhero.Alias = 'Ironman') AND [Power].Name = 'Flying'
			);

INSERT INTO [dbo].[SuperHeroPower]
			([SuperHeroID],
			[PowerID])(
			SELECT [Superhero].[ID], [Power].[ID]
			FROM [dbo].[Superhero], [dbo].[Power] 
			WHERE Superhero.Alias = 'Ironman' AND [Power].Name = 'Laser'
			);

INSERT INTO [dbo].[SuperHeroPower]
			([SuperHeroID],
			[PowerID])(
			SELECT [Superhero].[ID], [Power].[ID]
			FROM [dbo].[Superhero], [dbo].[Power] 
			WHERE [Power].Name = 'Super Smash'
			);

			
GO


