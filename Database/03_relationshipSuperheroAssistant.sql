USE [SuperHeroDB]
GO

/****** Object:  Table [dbo].[Assistant]    Script Date: 24/08/2021 16:16:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER TABLE [dbo].[Assistant]  WITH CHECK ADD  CONSTRAINT [FK_Assistant_Superhero] FOREIGN KEY([SuperHeroID])
REFERENCES [dbo].[Superhero] ([ID])
GO

ALTER TABLE [dbo].[Assistant] CHECK CONSTRAINT [FK_Assistant_Superhero]
GO


