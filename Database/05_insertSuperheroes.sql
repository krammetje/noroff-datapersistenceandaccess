USE [SuperHeroDB]
GO

INSERT INTO [dbo].[Superhero]
           ([Name]
           ,[Alias]
           ,[Origin])
     VALUES
           ('Peter Parker',
           'Spiderman',
           'Spiderbite'),

		   ('Anthony Edward Stark',
           'Ironman',
           'Money and brains'),

		   ('Thor, God of Thunder',
           'Thor',
           'Asgard')
GO


