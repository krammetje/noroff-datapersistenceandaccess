﻿using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DataPersistenceAndAccess
{
    class Program
    {
        static void Main(string[] args)
        {

            // 1) Read all the customers from the database
            List<Customers> customersAll = new List<Customers>();
            customersAll = SqlReader.GetCustomers();
            Console.WriteLine(@"1)");
            Console.WriteLine($"Number of customers in the database: {customersAll.Count}");
            Console.WriteLine($"One such Customer \n" +
                              $"Id: {customersAll[1].Id} \n" +
                              $"FirstName: {customersAll[1].FirstName}, \n" +
                              $"LastName: {customersAll[1].LastName}, \n" +
                              $"Country: {customersAll[1].Country}, \n" +
                              $"PostalCode: {customersAll[1].PostalCode}, \n" +
                              $"Phone: {customersAll[1].Phone}, \n" +
                              $"Email: {customersAll[1].Email}");
            Console.WriteLine("\n");

            // 2) Read a customer by their id
            Customers customerById = SqlReader.GetCustomers(10);
            Console.WriteLine("2)");
            Console.WriteLine($"Customer with id 10 \n" +
                  $"Id: {customerById.Id}, \n" +
                  $"FirstName: {customerById.FirstName}, \n" +
                  $"LastName: {customerById.LastName}, \n" +
                  $"Country: {customerById.Country}, \n" +
                  $"PostalCode: {customerById.PostalCode}, \n" +
                  $"Phone: {customerById.Phone}, \n" +
                  $"Email: {customerById.Email}");
            Console.WriteLine("\n");

            // 3) Read a customer by name
            Customers customerByName = SqlReader.GetCustomers("Marc");
            Console.WriteLine("3)");
            Console.WriteLine($"Customer named Marc \n" +
                  $"Id: {customerByName.Id}, \n" +
                  $"FirstName: {customerByName.FirstName}, \n" +
                  $"LastName: {customerByName.LastName}, \n" +
                  $"Country: {customerByName.Country}, \n" +
                  $"PostalCode: {customerByName.PostalCode}, \n" +
                  $"Phone: {customerByName.Phone}, \n" +
                  $"Email: {customerByName.Email}");
            Console.WriteLine("\n");

            // 4) Return a subset of the customer table with offset and limit
            List<Customers> customersSubset = new List<Customers>();
            customersSubset = SqlReader.GetCustomers(10,22);
            Console.WriteLine("4)");
            Console.WriteLine($"Number of customers in the subset: {customersSubset.Count}");
            Console.WriteLine($"The first one in the list \n" +
                              $"Id: {customersSubset[0].Id} \n" +
                              $"FirstName: {customersSubset[0].FirstName}, \n" +
                              $"LastName: {customersSubset[0].LastName}, \n" +
                              $"Country: {customersSubset[0].Country}, \n" +
                              $"PostalCode: {customersSubset[0].PostalCode}, \n" +
                              $"Phone: {customersSubset[0].Phone}, \n" +
                              $"Email: {customersSubset[0].Email}");
            Console.WriteLine();
            Console.WriteLine($"The last one in the list \n" +
                              $"Id: {customersSubset.Last().Id} \n" +
                              $"FirstName: {customersSubset.Last().FirstName}, \n" +
                              $"LastName: {customersSubset.Last().LastName}, \n" +
                              $"Country: {customersSubset.Last().Country}, \n" +
                              $"PostalCode: {customersSubset.Last().PostalCode}, \n" +
                              $"Phone: {customersSubset.Last().Phone}, \n" +
                              $"Email: {customersSubset.Last().Email}");
            Console.WriteLine("\n");

            // 5) Add a customer to the databse
            Customers customerToAdd = new Customers()
            {
                FirstName = "Markus",
                LastName = "Tester",
                Country = "Netherlands",
                PostalCode = "1000 AA",
                Phone = "06-123456789",
                Email = "markus_tester@noroff.com"
            };
            SqlReader.AddCustomer(customerToAdd);
            Customers customerToAddFromDb = SqlReader.GetCustomers("Markus");
            Console.WriteLine("5)");
            Console.WriteLine($"Customer we just added \n" +
                  $"Id: {customerToAddFromDb.Id}, \n" +
                  $"FirstName: {customerToAddFromDb.FirstName}, \n" +
                  $"LastName: {customerToAddFromDb.LastName}, \n" +
                  $"Country: {customerToAddFromDb.Country}, \n" +
                  $"PostalCode: {customerToAddFromDb.PostalCode}, \n" +
                  $"Phone: {customerToAddFromDb.Phone}, \n" +
                  $"Email: {customerToAddFromDb.Email}");
            Console.WriteLine("\n");


            // 6) Update an existing customer 

            Customers customerToUpdate = new Customers()
            {
                FirstName = "Mats",
                Country = "Norway",
            };
            SqlReader.UpdateCustomer(customerToAddFromDb.Id, customerToUpdate);
            Customers customerToUpdateFromDb = SqlReader.GetCustomers(customerToUpdate.FirstName);
            Console.WriteLine("6)");
            Console.WriteLine($"Customer we just updated \n" +
                  $"Id: {customerToUpdateFromDb.Id}, \n" +
                  $"FirstName: {customerToUpdateFromDb.FirstName}, \n" +
                  $"LastName: {customerToUpdateFromDb.LastName}, \n" +
                  $"Country: {customerToUpdateFromDb.Country}, \n" +
                  $"PostalCode: {customerToUpdateFromDb.PostalCode}, \n" +
                  $"Phone: {customerToUpdateFromDb.Phone}, \n" +
                  $"Email: {customerToUpdateFromDb.Email}");
            Console.WriteLine("\n");

            //7 
            Console.WriteLine("7)");
            DataProcessing.CustomerDemographics(customersAll);
            Console.WriteLine("\n");

            //8 
            Console.WriteLine("8) ");
            List<CustomerSpender> spenders = SqlReader.GetCustomerSpenders();
            foreach (CustomerSpender spender in spenders)
            {
                Console.WriteLine($"Customer with id: {spender.CustomerId} spent: {spender.Spent}");
            }
            Console.WriteLine("\n");



            //9 
            Console.WriteLine("9) ");
            Console.WriteLine("Favorite genre without ties");
            Console.WriteLine($"Customer 23");
            List<CustomerGenre> genres = SqlReader.GetCustomerMostPopularGenres(23);
            foreach (CustomerGenre genre in genres)
            {
                Console.WriteLine($"{genre.Genre}: {genre.Count}");
            }
            Console.WriteLine();

            Console.WriteLine("Favorite genre with ties");
            Console.WriteLine($"Customer 12");
            genres = SqlReader.GetCustomerMostPopularGenres(12);
            foreach (CustomerGenre genre in genres)
            {
                Console.WriteLine($"{genre.Genre}: {genre.Count}");
            }
            Console.WriteLine();

        }          
    }
}
