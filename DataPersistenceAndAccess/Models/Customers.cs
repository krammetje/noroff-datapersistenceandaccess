﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataPersistenceAndAccess
{
    public class Customers
    {
        public int Id; //0
        public string FirstName; //1
        public string LastName; //2
        public string Country; //7
        public string PostalCode; //8
        public string Phone; //9
        public string Email; //11
    }
}
