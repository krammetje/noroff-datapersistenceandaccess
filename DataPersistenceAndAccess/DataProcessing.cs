﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace DataPersistenceAndAccess
{
    public class DataProcessing
    {
        /// <summary>
        /// Gets a list of customers and returns a CustomerCountry list with how many customers are from what country.
        /// </summary>
        /// <param name="customers"></param>
        /// <returns>A list containing how many customers are from what country.</returns>
        public static List<CustomerCountry> CustomerDemographics(List<Customers> customers)
        {
            //A dictionary is used to store the country data.
            Dictionary<string, int> customerDemographicsDict = new Dictionary<string, int>();
            foreach (Customers customer in customers)
            {
                if (customerDemographicsDict.ContainsKey(customer.Country))
                {
                    customerDemographicsDict[customer.Country] += 1;
                }
                else
                {
                    customerDemographicsDict.Add(customer.Country, 1);
                }
            }

            var sortedDict = from entry in customerDemographicsDict orderby entry.Value descending select entry;

            List<CustomerCountry> customerDemographics = new List<CustomerCountry>();
            foreach (KeyValuePair<string,int> kvp in sortedDict)
            {
                customerDemographics.Add(new CustomerCountry() { Country = kvp.Key, count = kvp.Value });
            }

            foreach(CustomerCountry test in customerDemographics)
            {
                Console.WriteLine($"Country {test.Country}: {test.count}");
            }

            return customerDemographics;
        }

    }
}
