﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Data.SqlClient;
using System.Linq;

namespace DataPersistenceAndAccess
{
    public static class SqlReader
    {
        /// <summary>
        /// Builds SqlConnectionStringBuilder.
        /// </summary>
        /// <returns>A builder object with the required variables.</returns>
        public static SqlConnectionStringBuilder SetBuilder()
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.DataSource = @"localhost";
            builder.InitialCatalog = "Chinook";
            builder.IntegratedSecurity = true;
            return builder;
        }

        /// <summary>
        /// Get the most popular genre a specific customer listens to using their Id. Returns multiple genres when tied.
        /// </summary>
        /// <returns>A list of CustomerGenre containing the most popular genres from a specific user.</returns>
        public static List<CustomerGenre> GetCustomerMostPopularGenres(int customerId)
        {
            List<CustomerGenre> customerGenresList = new List<CustomerGenre>();
            using (SqlConnection connection = new SqlConnection(SetBuilder().ConnectionString))
            {
                connection.Open();
                string sql = @"SELECT g.Name 
                               FROM Customer c
                               INNER JOIN Invoice i
                               ON i.CustomerId = @customerId AND i.customerId = c.CustomerId
                               INNER JOIN InvoiceLine il
                               ON il.invoiceId = i.invoiceId
                               INNER JOIN Track t
                               ON t.TrackId = il.TrackId
                               INNER JOIN Genre g
                               ON g.GenreId = t.GenreId";

                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    command.Parameters.AddWithValue("@customerId", customerId);

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            if (customerGenresList.Exists(x => x.Genre == SafeGetString(reader, 0)))
                            {
                                customerGenresList.Find(x => x.Genre == SafeGetString(reader, 0)).Count += 1;
                            }
                            else
                            {
                                customerGenresList.Add(new CustomerGenre()
                                {
                                    Genre = SafeGetString(reader, 0),
                                    Count = 1
                                });
                            }
                        }
                    }
                }
            }

            List<CustomerGenre> sortList = customerGenresList.OrderByDescending(x => x.Count).ToList();
            List<CustomerGenre> favoriteGenre = new List<CustomerGenre>();
            foreach (CustomerGenre item in sortList)
            {
                if (item.Count == sortList.First().Count)
                {
                    favoriteGenre.Add(item);
                }
                else
                {
                    break;
                }

            }
            return favoriteGenre;
        }

        /// <summary>
        /// Get a list of all customers and how much they have spent as CustomerSpender.
        /// </summary>
        /// <returns>A list of CustomerSpender which includes the customer Id and how much they have spent.</returns>
        public static List<CustomerSpender> GetCustomerSpenders()
        {
            List <CustomerSpender> customerSpenderList = new List<CustomerSpender>();
            using (SqlConnection connection = new SqlConnection(SetBuilder().ConnectionString))
            {
                connection.Open();
                string sql = @"SELECT c.CustomerId, i.Total 
                               FROM Customer c
                               INNER JOIN Invoice i
                               ON c.CustomerId = i.CustomerId";
                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            if (customerSpenderList.Exists(x => x.CustomerId == reader.GetInt32(0)))
                            {
                                customerSpenderList.Find(x => x.CustomerId == reader.GetInt32(0)).Spent += (double)reader.GetDecimal(1);
                            }
                            else
                            {
                                customerSpenderList.Add(new CustomerSpender()
                                {
                                    CustomerId = reader.GetInt32(0),
                                    Spent = (double)reader.GetDecimal(1)
                                });
                            }
                        }
                    }
                    List<CustomerSpender> sortList = customerSpenderList.OrderByDescending(x => x.Spent).ToList();
                    foreach(CustomerSpender spender in sortList)
                    {
                        spender.Spent = Math.Round(spender.Spent, 2); //Rounds spent to .##
                    }
                    return sortList;
                }
            }
        }

        /// <summary>
        /// Returns a list of all customers.
        /// </summary>
        /// <returns>A list of all customers.</returns>
        public static List<Customers> GetCustomers()
        {
            List<Customers> customers = new List<Customers>();

            using (SqlConnection connection = new SqlConnection(SetBuilder().ConnectionString)) {
                connection.Open();
                string sql = "SELECT * FROM Customer";

                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            customers.Add(new Customers() {
                                Id = reader.GetInt32(0),
                                FirstName = SafeGetString(reader, 1),
                                LastName = SafeGetString(reader, 2),
                                Country = SafeGetString(reader, 7),
                                PostalCode = SafeGetString(reader, 8),
                                Phone = SafeGetString(reader, 9),
                                Email = SafeGetString(reader, 11)
                            });
                        }
                    }
                }
            }
            return customers;
        }

        /// <summary>
        /// Returns the customer with the given Id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>The customer with given Id</returns>
        public static Customers GetCustomers(int id)
        {
            using (SqlConnection connection = new SqlConnection(SetBuilder().ConnectionString))
            {
                connection.Open();
                string sql = $"SELECT * FROM Customer WHERE CustomerId={id}";

                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        reader.Read();
                        return new Customers()
                        {
                            Id = reader.GetInt32(0),
                            FirstName = SafeGetString(reader, 1),
                            LastName = SafeGetString(reader, 2),
                            Country = SafeGetString(reader, 7),
                            PostalCode = SafeGetString(reader, 8),
                            Phone = SafeGetString(reader, 9),
                            Email = SafeGetString(reader, 11)
                        };
                    }
                }
            }
        }

        /// <summary>
        /// Returns the first customer from the database with given name.
        /// </summary>
        /// <param name="firstName"></param>
        /// <returns>The first customer from the database with given name.</returns>
        public static Customers GetCustomers(string firstName)
        {
            using (SqlConnection connection = new SqlConnection(SetBuilder().ConnectionString))
            {
                connection.Open();
                string sql = "SELECT * FROM Customer WHERE FirstName LIKE @firstName";

                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    command.Parameters.AddWithValue("@firstName", firstName);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        reader.Read();
                        return new Customers()
                        {
                            Id = reader.GetInt32(0),
                            FirstName = SafeGetString(reader, 1),
                            LastName = SafeGetString(reader, 2),
                            Country = SafeGetString(reader, 7),
                            PostalCode = SafeGetString(reader, 8),
                            Phone = SafeGetString(reader, 9),
                            Email = SafeGetString(reader, 11)
                        };
                    }
                }
            }
        }


        /// <summary>
        /// Returns a list of customers with the offset and limit
        /// </summary>
        /// <param name="offset"></param>
        /// <param name="limit"></param>
        /// <returns>A list of customers with the offset and limit</returns>
        public static List<Customers> GetCustomers(uint offset, uint limit)
        {
            List<Customers> customers = new List<Customers>();

            if (limit > 0)
            {
                using (SqlConnection connection = new SqlConnection(SetBuilder().ConnectionString))
                {
                    connection.Open();
                    string sql = "SELECT * FROM Customer ORDER BY CustomerId OFFSET @offset ROWS FETCH NEXT @limit ROWS ONLY";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@limit", (int)limit);
                        command.Parameters.AddWithValue("@offset", (int)offset);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                customers.Add(new Customers()
                                {
                                    Id = reader.GetInt32(0),
                                    FirstName = SafeGetString(reader, 1),
                                    LastName = SafeGetString(reader, 2),
                                    Country = SafeGetString(reader, 7),
                                    PostalCode = SafeGetString(reader, 8),
                                    Phone = SafeGetString(reader, 9),
                                    Email = SafeGetString(reader, 11)
                                });
                            }
                        }
                    }
                }
            }
            return customers;
        }

        /// <summary>
        /// Insert a customer into the Customer table.
        /// </summary>
        /// <param name="customer"></param>
        public static void AddCustomer(Customers customer)
        {
            using (SqlConnection connection = new SqlConnection(SetBuilder().ConnectionString))
            {
                connection.Open();
                string sql = @"INSERT INTO Customer (FirstName, LastName, Country, PostalCode, Phone, Email) 
                                VALUES (@FirstName, @LastName, @Country, @PostalCode, @Phone, @Email)";

                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    command.Parameters.AddWithValue("@FirstName", customer.FirstName);
                    command.Parameters.AddWithValue("@LastName", customer.LastName);
                    command.Parameters.AddWithValue("@Country", customer.Country);
                    command.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                    command.Parameters.AddWithValue("@Phone", customer.Phone);
                    command.Parameters.AddWithValue("@Email", customer.Email);

                    command.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// Updates the fields of specific customer with Id with the fields of the given customer object.
        /// Empty/missing fields in the given customer object are ignored and won't override the data in the database.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="customer"></param>
        public static void UpdateCustomer(int id, Customers customer)
        {
            using (SqlConnection connection = new SqlConnection(SetBuilder().ConnectionString))
            {
                connection.Open();


                Customers customerToUpdate = GetCustomers(id);

                string sql = @"UPDATE Customer SET FirstName = @FirstName, 
                                                   LastName = @LastName, 
                                                   Country = @Country,
                                                   PostalCode = @PostalCode, 
                                                   Phone = @Phone, 
                                                   Email = @Email 
                                               WHERE CustomerId = @Id";

                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    //If no new value is given, takes the value from the database.
                    if (customer.FirstName != string.Empty && customer.FirstName != null)
                    {
                        command.Parameters.AddWithValue("@FirstName", customer.FirstName);
                    }
                    else
                    {
                        command.Parameters.AddWithValue("@FirstName", customerToUpdate.FirstName);
                    }
                    if (customer.LastName != string.Empty && customer.LastName != null)
                    {
                        command.Parameters.AddWithValue("@LastName", customer.LastName);
                    }
                    else
                    {
                        command.Parameters.AddWithValue("@LastName", customerToUpdate.LastName);
                    }
                    if (customer.Country != string.Empty && customer.Country != null)
                    {
                        command.Parameters.AddWithValue("@Country", customer.Country);
                    }
                    else
                    {
                        command.Parameters.AddWithValue("@Country", customerToUpdate.Country);
                    }
                    if (customer.PostalCode != string.Empty && customer.PostalCode != null)
                    {
                        command.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                    }
                    else
                    {
                        command.Parameters.AddWithValue("@PostalCode", customerToUpdate.PostalCode);
                    }
                    if (customer.Phone != string.Empty && customer.Phone != null)
                    {
                        command.Parameters.AddWithValue("@Phone", customer.Phone);
                    }
                    else
                    {
                        command.Parameters.AddWithValue("@Phone", customerToUpdate.Phone);
                    }
                    if (customer.Email != string.Empty && customer.Email != null)
                    {
                        command.Parameters.AddWithValue("@Email", customer.Email);
                    }
                    else
                    {
                        command.Parameters.AddWithValue("@Email", customerToUpdate.Email);
                    }

                    command.Parameters.AddWithValue("@Id", id);
                    command.ExecuteNonQuery();

                }
            }
        }


        /// <summary>
        /// Checks if the entry is not null. Returns empty string if entry is null.
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="colIndex"></param>
        /// <returns></returns>
        static string SafeGetString(SqlDataReader reader, int colIndex)
        {
            if (!reader.IsDBNull(colIndex))
            {
                return reader.GetString(colIndex);
            }
            return string.Empty;
        }
    }
}
