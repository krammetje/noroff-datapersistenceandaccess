# Assignment 2 - Data access with SQL Client

## Appendix A

The database folder contains 9 sql files and the database Diagram.

01). Create the database

02). Create the tables in the database

03). Set the relationship between the superhero and assistant table

04). Set the relationship between the superhero and power table

05). Create 3 new super heroes and put these in the Superhero table

06). Create 3 new assistants and put these in the Assistant table

07). Create 4 powers, put these in the Power table and assign these to the heroes

08). Update the name of a superhero

09). Delete one of the assistants from the Assistant table


## Appendix B

A .NET project used to connect to a database and read and modify the data.
Default database address is set to local host.

### Main Functions
*GetCustomers* - Returns the data of the customer or all customers from the Customer Table.

*AddCustomer* - Adds a new customer to the Customer Table.

*UpdateCustomer* - Updates the value of a specific customer in the Customer Table.

*GetCustomerSpenders* - Returns a list of customers and the ammount they have spent.

*GetCustomerMostPopularGenres* Returns the most popular genre of a specific customer depending on how often they bought it. Returns multiple items on ties.

*CustomerDemographics* Returns a list of the different countries and how many customers are from each country, sorted in descending order.